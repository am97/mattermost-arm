# mattermost-arm
[Mattermost](https://www.mattermost.org/) 5.2.1 armv7l binary and build instructions

_Binaries in this repo were built on an Olimex Lime 2 with Armbian 5.38_

# Build mattermost-server binary
_Based on https://developers.mattermost.com/contribute/server/developer-setup/_  
1. Install Go: 
	1. Download and extract the latest Go for armv6l available on https://golang.org/dl/  
        (armv6 binaries work in armv7)  
        Example:

        ```
        wget https://dl.google.com/go/go1.10.3.linux-armv6l.tar.gz
        tar --directory=/usr/local -xzf go1.10.3.linux-armv6l.tar.gz
        ```
	2. Add the following lines to your `~/.bashrc`  
        
        ```
        export GOPATH=$HOME/go
        export PATH=$PATH:$GOPATH/bin
        export PATH=$PATH:/usr/local/go/bin
        ulimit -n 8096
        ```
	3. Run `source ~/.bashrc`
2. Set up Go directories:
    
    ```
    mkdir -p ~/go/src/github.com/mattermost
    cd ~/go/src/github.com/mattermost
    ```
3. Clone the `mattermost-server` source code:
    ```
    git clone https://github.com/mattermost/mattermost-server
    cd mattermost-server
    ```
4. Checkout to a stable version:
	```
	git checkout v5.2.1
	```
4. In `Makefile`, replace
    ```
    BUILD_ENTERPRISE ?= true
    ```
    
    with
    
    ```
    BUILD_ENTERPRISE ?= false
    ```

4. In `./build/release.mk`, replace this lines
    ```
    @echo Build Linux amd64
    env GOOS=linux GOARCH=amd64 $(GO) install -i $(GOFLAGS) $(GO_LINKER_FLAGS) ./...
    ```
    
    with
    ```
    @echo Build Linux arm
    env GOOS=linux GOARCH=arm GOARM=7 $(GO) install -i $(GOFLAGS) $(GO_LINKER_FLAGS) ./...
    ```
5. Build mattermost-server
    ```
    cd ~/go/src/github.com/mattermost/mattermost-server
    make build-linux
    ```
6. This will create 3 files in `~/go/bin`:
    - interface_generator
    - mattermost
    - platform
# Build Mattermost Docker image
1. Download the `mattermost-docker` sources: 
	```
	cd ~
	git clone https://github.com/mattermost/mattermost-docker
	```
2. Download a Mattermost release (same version that `mattermost-server`): 
	```
    wget https://releases.mattermost.com/5.2.1/mattermost-5.2.1-linux-amd64.tar.gz
    tar --directory=~/mattermost-docker/app -xzf mattermost-5.2.1-linux-amd64.tar.gz
    ```
3. Replace files in `~/mattermost-docker/app/mattermost/bin/` with the ones you built:
	```
	cp ~/go/bin/mattermost ~/mattermost-docker/app/mattermost/bin/
	cp ~/go/bin/platform ~/mattermost-docker/app/mattermost/bin/
	```
4. In `~/mattermost-docker/app/Dockerfile`, replace this lines
	```
	# Get Mattermost
	RUN mkdir -p /mattermost/data \
		&& if [ "$edition" = "team" ] ; then curl https://releases.mattermost.com/$MM_VERSION/mattermost-team-$MM_VERSION-linux-amd64.tar.gz | tar -xvz ; \
		  else curl https://releases.mattermost.com/$MM_VERSION/mattermost-$MM_VERSION-linux-amd64.tar.gz | tar -xvz ; fi \
		&& cp /mattermost/config/config.json /config.json.save \
		&& rm -rf /mattermost/config/config.json
	```

	with

	```
	# Get Mattermost
	COPY mattermost/ /mattermost/
	RUN mkdir -p /mattermost/data \
		&& cp /mattermost/config/config.json /config.json.save \
		&& rm -rf /mattermost/config/config.json
	```
5. In `docker-compose.yml`, remove the `web` container. (Or you can leave it, but I have not tested the build)
6. Build the images
	```
	cd ~/mattermost-docker/
	docker-compose build
	```
	Note: there is no need to uncomment these lines:
	```
	# args:
	#   - edition=team
	```
	because we no longer use the `edition` env variable.
7. `docker images` should now show 2 new images:
	- mattermostdocker_app
	- mattermostdocker_db

# Example deploy (insecure)
1. In `docker-compose.yml`, add the following lines under the `app:` section:

	```
    ports:
    - "8000:8000"
	```
2. Run `docker-compose up -d`
3. Now you can access your mattermost instance at the address \<your-server-ip>:8000
